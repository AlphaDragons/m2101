#include "D:\Cours\M2101\TP\m2101\Digicode\PIC C\main.h"
#define ligne1 input(PIN_B4)
#define ligne2 input(PIN_B5)
#define ligne3 input(PIN_B6)
#define ligne4 input(PIN_B7)

int32 codeTyped = 0;
int32 code = 1243;
int change = 0;

void verif(int32 c, int32 code);
void changeCode(int32 c);

#int_EXT
void  EXT_isr(void) 
{
   disable_interrupts(INT_EXT);
   
   output_d(1);
   if(ligne1){
      printf("1");
      codeTyped*=10;
      codeTyped+=1;
   }
   if(ligne2){
      printf("4");
      codeTyped*=10;
      codeTyped+=4;
   }
   if(ligne3){
      printf("7");
      codeTyped*=10;
      codeTyped+=7;
   }
   if(ligne4){
      printf("*");
   }
   
   output_d(2);
   if(ligne1){
      printf("2");
      codeTyped*=10;
      codeTyped+=2;
   }
   if(ligne2){
      printf("5");
      codeTyped*=10;
      codeTyped+=5;
   }
   if(ligne3){
      printf("8");
      codeTyped*=10;
      codeTyped+=8;
   }
   if(ligne4){
      printf("0");
      codeTyped*=10;
   }
   
   output_d(4);
   if(ligne1){
      printf("3");
      codeTyped*=10;
      codeTyped+=3;
   }
   if(ligne2){
      printf("6");
      codeTyped*=10;
      codeTyped+=6;
   }
   if(ligne3){
      printf("9");
      codeTyped*=10;
      codeTyped+=9;
   }
   if(ligne4){
      printf("SERGI!!!");
      printf("%lu",codeTyped);
      if(!change){
         verif(codeTyped, code);
      }else{
         changeCode(codeTyped); 
      }
      codeTyped=0;
   }
   
   output_d(7);
   enable_interrupts(INT_EXT);
}

void verif(int32 c, int32 code){
   printf(" %lu ",c);
   if(c == code){
      printf("OK");
   }else{
      printf("Pas OK");
   }
   
   if(c == 9999){
      change = 1;
   }
}

void changeCode(int32 c){
   code = c;
   change = 0;
}


void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);
   //Setup_Oscillator parameter not selected from Intr Oscillator Config tab
   
   // TODO: USER CODE!!
   output_d(7);
   while(true){
      //output_d(7);
      /*output_d(1);
      output_d(2);
      output_d(4);*/
   }

}
