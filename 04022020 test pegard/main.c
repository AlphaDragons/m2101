#include "D:\Cours\Programmes\04022020 test pegard\main.h"
int8 sec,min,hour,cent;
int1 flag;
#int_TIMER1
void  TIMER1_isr(void) 
{
   set_timer1(15536);
   cent++;
   if(cent==100){
      sec++;
      cent = 0;
   }
   if(sec==60){
      min++;
      sec = 0;
   }
   if(min==60){
      hour++;
      min = 0;
   }
}

#int_EXT
void  EXT_isr(void) 
{
   cent=sec=min=hour=0;
}

#int_EXT1
void  EXT1_isr(void) 
{
   flag=!flag;
   if(flag){
      enable_interrupts(INT_TIMER1);
   }else{
      disable_interrupts(INT_TIMER1);
   }
}



void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
   setup_timer_2(T2_DISABLED,0,1);
   enable_interrupts(INT_TIMER1);
   enable_interrupts(INT_EXT);
   enable_interrupts(INT_EXT1);
   enable_interrupts(GLOBAL);
//Setup_Oscillator parameter not selected from Intr Oscillator Config tab

   // TODO: USER CODE!!
   
   while(true){
      printf("Temps ecoule %u H %u M %u S %u C\n\r",hour,min,sec,cent);
      output_d(cent%10);
      output_high(pin_e2);
      delay_us(5);
      output_low(pin_e2);
      
      output_d(cent/10);
      output_high(pin_e1);
      delay_us(5);
      output_low(pin_e1);
      
      output_d(sec%10);
      output_high(pin_e0);
      delay_us(5);
      output_low(pin_e0);
      
      output_d(sec/10);
      output_high(pin_c5);
      delay_us(5);
      output_low(pin_c5);
      
      output_d(min%10);
      output_high(pin_c4);
      delay_us(5);
      output_low(pin_c4);
      
      output_d(min/10);
      output_high(pin_c3);
      delay_us(5);
      output_low(pin_c3);
      
   }
}
