#include "D:\Cours\M2101\TP\m2101\TP Alarme\PIC C\main.h"
#define b1 input(PIN_B1)
#define b2 input(PIN_B2)
#define b3 input(PIN_B3)
#define b4 input(PIN_B4)
#define b5 input(PIN_B5)
#define b6 input(PIN_B6)
#define buzzer output_a

int32 timerdix = 0, timer = 0, timerStart = 0;

#int_TIMER1
void  TIMER1_isr(void) 
{
   if(timerStart = 1){
      set_timer1(3036);
      timerdix++;
      if(timerdix>10){
         timerdix=timerdix%10;
         timer++;
      }
      timer=timer%500; 
   }
}

#int_EXT
void  EXT_isr(void) 
{

}

int instant(){
   int bool = 0;
   if(!b1 || !b2 || !b3 || !b4){
      bool = 1;
   }
   return bool;
}

int retarded(){
   int bool = 0;
   if(!b5 || !b6){
      bool = 1;
   }
   return bool;
}

void retardLaunch(){
   timerStart = 1;
   while(timer<20){
      buzzer(1);
   }
   buzzer(0);
   timerStart = 0;
   timer = 0;
}



void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);
   setup_timer_2(T2_DISABLED,0,1);
   setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
   enable_interrupts(INT_TIMER1);
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);
//Setup_Oscillator parameter not selected from Intr Oscillator Config tab

   // TODO: USER CODE!!
   retardLaunch();
   while(true){
   }
}
