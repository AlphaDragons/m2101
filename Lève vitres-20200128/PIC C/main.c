#include "D:\Cours\M2101\TP\L�ve vitres-20200128\PIC C\main.h"
#define remonteComplet INPUT(PIN_B0)
#define remonte INPUT(PIN_B1)
#define descend INPUT(PIN_B2)
#define descendComplet INPUT(PIN_B3)
#define FCH INPUT(PIN_B6)
#define FCB INPUT(PIN_B7)
#define reset() output_d(0b00)
#define monter() output_d(0b01)
#define descendre() output_d(0b11)

void remonterAFond(){
   while(!FCH){
            monter();
         }
   reset();
}


void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
//Setup_Oscillator parameter not selected from Intr Oscillator Config tab

   // TODO: USER CODE!!
   
   while(true){
      if(remonteComplet && !descendComplet){
         while(!FCH){
            monter();
         }
         reset();
      }else if(descendComplet){
         while(!FCB){
            descendre();
         }
         reset();
      }
      while(remonte && !FCH && !descend){
         monter();
      }
      while(descend && !FCB){
         descendre();
      }
      reset();
   }

}
